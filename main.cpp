#include <QString>

#include <iostream>
#include <string>

int main()
{
    QString hello("Hello World");
    std::cout << hello.toStdString() << std::endl;
    return 0;
}